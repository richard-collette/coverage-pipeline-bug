#!/usr/bin/env sh
#If the image already exists, exit
set -x

imageName=$1

echo "Searching for docker image ${imageName}"
result=$(docker search ${imageName})
echo "Search result: ${result}"

found=$(docker search ${imageName} | grep ${imageName} | wc -l)
echo "Find result: ${found}"
if [[ ${found} == '0' ]]
  then
    echo "Creating docker image ${imageName}"
    docker build -t ${imageName} --target base .
    docker push ${imageName}
  fi
