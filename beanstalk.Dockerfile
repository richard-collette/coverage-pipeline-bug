FROM nginx:1.15.8-alpine as host
COPY dist/coverage-pipeline-bug /usr/share/nginx/html
COPY default.conf /etc/nginx/default.conf
WORKDIR /usr/share/nginx/html
EXPOSE 80
