FROM node:10.15.0 as base
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get -y update && apt-get -y upgrade && apt-get install -y google-chrome-stable

FROM base as lint_test_build
WORKDIR /opt/app
ADD package.json package-lock.json /opt/app/
RUN npm i
COPY . .
RUN npm run ci

FROM nginx:1.15.8-alpine as host
COPY --from=lint_test_build /opt/app/dist/coverage-pipeline-bug /usr/share/nginx/html
COPY --from=lint_test_build /opt/app/default.conf /etc/nginx/default.conf
WORKDIR /usr/share/nginx/html
EXPOSE 80
